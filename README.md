# Play
A command line tool to play music.


# Install
```bash
wget https://gitlab.com/HttpAnimations/play/-/raw/main/play.py?ref_type=heads && mv play.py?ref_type=heads play.py && wget https://gitlab.com/HttpAnimations/play/-/raw/main/play?ref_type=heads && mv play?ref_type=heads play && sudo mv play /usr/local/bin && sudo mv play.py /usr/local/bin
```