import sys
import pygame
import os

def play_music(file_path):
    try:
        # Initialize Pygame mixer
        pygame.mixer.init()

        # Load the music file
        pygame.mixer.music.load(file_path)

        # Get the filename from the file path
        file_name = os.path.basename(file_path)
        print(f"Now playing: {file_name}")

        # Play the music
        pygame.mixer.music.play()

        # Wait for music to finish playing
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)  # Check every 10 milliseconds

    except pygame.error as e:
        print(f"Error occurred while playing audio: {e}")
    finally:
        pygame.mixer.quit()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: play <path_to_audio_file>")
        sys.exit(1)

    audio_file = sys.argv[1]
    play_music(audio_file)
